# SixtySecondsCV

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gh/LaGuer/SixtySecondsCV/master)

1. From MyBinder
- Wait for jupyter to start ...
- Once started, Select New : Terminal from Jupyter
- from the shell prompt type ```xelatex --shell-escape SixtySecondsCV.tex```
- Press Enter as many times as required
- PDF is generated and ready for download .


2. A link to an online automatic latex build system: [latexonline.cc](https://latexonline.cc/compile?git=https%3A%2F%2Fgithub.com%2Flaguer%2Fsixtysecondscv&target=sixtysecondscv.tex&command=pdflatex), [latexbase.com](https://latexbase.com) or try [OverLeaf](https://www.overleaf.com/latex/templates/sixtysecondscv/gcdrzwwvkqcr)

| Latex Doc Type                |      "*.cls"                 |        "*.tex"              |      Compile in PDF                                                                                                                                    |
| ----------------------------- |:----------------------------:|:---------------------------:|-------------------------------------------------------------------------------------------------------------------------------------------------------:|
|SixtySecondsCV Format  |[sixtysecondscv.cls](sixtysecondscv.cls)| [sixtysecondscv.tex](sixtysecondscv.tex )       |[latexonline.cc main.tex](https://latexonline.cc/compile?git=https%3A%2F%2Fgithub.com%2FLaGuer%2Fsixtysecondscv&target=sixtysecondscv.tex&command=pdflatex)      |


